# Minerva

Uma solução informatizada para o escalonamento de professores nas escolas brasileiras.


## Dados Gerais

- **Nome do Projeto**: Minerva;
- **Aluno**: [Léo Marco de Assis Hardt](http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K2463923J2);
- **Professor Orientador**: [Gustavo Neuberger](http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4764159Y6);
- **Ano do Projeto**: 2020;
- **Licença**: A definir.

## Links Rápidos

- [Parte Escrita](texto.pdf);
- [Wiki](https://github.com/lhardt/Minerva/wiki)
